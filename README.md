# Dotfiles

dependencies: (these deps apply to the entire repo, xmonad-config has some additional deps mentioned there)
  1. powerline - for the powerline appearance in bash
  2. powerline-vim - vim plugin of powerline
  4. pywal - colortheme (used in i3, xmonad, qtile, vim, bg.sh)
  5. nitrogen - for bg.sh, ranwal.sh and .xprofile(can simply be removed from here)
  6. picom - compositor (remove if unwanted)

Archicon for Qtile:
- from the BeautyLine Icon Theme https://store.kde.org/p/1425426/
